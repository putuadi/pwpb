<?php

require("koneksi.php");

$ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel ORDER BY id DESC");

$kumpulan_data = [];

while( $data = mysqli_fetch_assoc($ambil_data) ){
    $kumpulan_data[] = $data;
}

if( isset($_POST["submit"]) ){
    $username = $_POST["username"];
    if( mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT username FROM user_artikel WHERE username='$username'")) ){
        echo "<script>alert('Sudah ada yang memakai username itu')</script>";
    }else{
        $nama = $_POST["nama"];
        $username = $_POST["username"];
        $password = $_POST["password"];

        $nama_gambar = $_FILES["gambar"]["name"];
        $tmp_name = $_FILES["gambar"]["tmp_name"];

        $ekstensi_gambar = explode('.', $nama_gambar);
        $ekstensi_gambar = strtolower(end($ekstensi_gambar));

        $nama_baru = uniqid();
        $nama_baru .= '.';
        $nama_baru .= $ekstensi_gambar;

        $nama_gambar_baru = $nama_baru;
        
        move_uploaded_file($tmp_name, "assets/user/" . $nama_gambar_baru);
        mysqli_query($koneksi, "INSERT INTO user_artikel VALUES('', '$nama_gambar_baru', '$nama', '$username', '$password')");
        
        $data_user_id = mysqli_query($koneksi, "SELECT id FROM user_artikel WHERE nama='$nama'");
        $data_user_id_ini = mysqli_fetch_assoc($data_user_id)["id"];
        echo"<script> alert('Berhasil registrasi')
            window.location.href = 'log-in.php';
        </script>";
    }
}


?>

<!doctype html>
<html lang="zxx">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/animate.min.css">

    <link rel="stylesheet" href="assets/fonts/flaticon.css">

    <link rel="stylesheet" href="assets/css/boxicons.min.css">

    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <link rel="stylesheet" href="assets/css/nice-select.min.css">

    <link rel="stylesheet" href="assets/css/meanmenu.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" href="assets/css/theme-dark.css">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">
    <title>Techex - Technology & IT Services HTML Template</title>
</head>

<body>

    <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="spinner"></div>
            </div>
        </div>
    </div>


    <header class="top-header top-header-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-6">
                    <div class="top-head-left">
                        <div class="top-contact">
                            <h3>Dibuat Oleh : <a href="">Adi & Labe</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="top-header-right">
                        <div class="top-header-social">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/" target="_blank">
                                        <i class='bx bxl-facebook'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/?lang=en" target="_blank">
                                        <i class='bx bxl-twitter'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/" target="_blank">
                                        <i class='bx bxl-linkedin-square'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <i class='bx bxl-instagram'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="language-list">
                            <select class="language-list-item">
                                <option>English</option>
                                <option>العربيّة</option>
                                <option>Deutsch</option>
                                <option>Português</option>
                                <option>简体中文</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="navbar-area">

        <div class="mobile-nav">
            <a href="index.php" class="logo">
                <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo" >
                <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light ">
                    <a class="navbar-brand" href="index.php">
                        <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo" width="260px">
                        <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo" width="260px">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item">
                                <a href="index.php" class="nav-link active">
                                    Beranda
                                </a>
                            </li>
                        </ul>
                        <div class="nav-side d-display">
                            <div class="nav-side-item">
                                <div class="search-box">
                                    <i class='bx bx-search'></i>
                                </div>
                            </div>
                            <div class="nav-side-item">
                                <div class="get-btn">
                                    <a href="log-in.php" class="default-btn btn-bg-two border-radius-50">Login
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="side-nav-responsive">
            <div class="container-max">
                <div class="dot-menu">
                    <div class="circle-inner">
                        <div class="in-circle circle-one"></div>
                        <div class="in-circle circle-two"></div>
                        <div class="in-circle circle-three"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="side-nav-inner">
                        <div class="side-nav justify-content-center align-items-center">
                            <div class="side-nav-item nav-side">
                                <div class="search-box">
                                    <i class='bx bx-search'></i>
                                </div>
                                <div class="get-btn">
                                    <a href="contact.html" class="default-btn btn-bg-two border-radius-50">Get A Quote
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="search-overlay">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-close">
                    <span class="search-close-line"></span>
                    <span class="search-close-line"></span>
                </div>
                <div class="search-form">
                    <form>
                        <input type="text" class="input-search" placeholder="Search here...">
                        <button type="submit"><i class='bx bx-search'></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="inner-banner">
        <div class="container">
            <div class="inner-title text-center">
                <h3>Registrasi</h3>
                <ul>
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <i class='bx bx-chevrons-right'></i>
                    </li>
                    <li>Registrasi</li>
                </ul>
            </div>
        </div>
        <div class="inner-shape">
            <img src="assets/images/shape/inner-shape.png" alt="Images">
        </div>
    </div>


    <div class="user-area pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="user-img">
                        <img src="assets/images/user-img.jpg" alt="Images">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="user-form">
                        <div class="contact-form">
                            <h2>Registrasi</h2>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="file" name="gambar" id="upload" class="form-control" required
                                                data-error="Upload gambar terlebih dahulu" accept="Image/*" />
                                            <div id="display-image"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 ">
                                        <div class="form-group">
                                            <input type="text" name="nama" class="form-control" required
                                                data-error=""
                                                placeholder="Tulis nama anda">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 ">
                                        <div class="form-group">
                                            <input type="email" name="username" class="form-control" required
                                                data-error="Please enter your Username or Email"
                                                placeholder="Tulis username anda">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input class="form-control" name="password" type="password" name="password"
                                                placeholder="Tulis Password anda">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 ">
                                        <button type="submit" name="submit" class="default-btn btn-bg-two">
                                            Registrasi
                                        </button>
                                    </div>
                                    <div class="col-12">
                                        <p class="account-desc">
                                            Sudah punya akun?
                                            <a href="log-in.php">Login Now</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


       <footer class="footer-area footer-bg">
        <div class="container">
            <div class="footer-top pt-100 pb-70">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="index.php">
                                    <img src="assets/images/logos/logoladi-white.png" alt="Images" width="260px">
                                </a>
                            </div>
                            <p>
                                Ladi.TV menyajikan berita breaking news dan berita terkini seputar nasional,
                                    internasional, politik, ekonomi,
                                    olahraga, dan entertainment
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="footer-widget pl-2">
                            <h3>Kategori</h3>
                            <ul class="footer-list">
                                <li>
                                    <a href='index.php'>
                                        <i class='bx bx-chevron-right'></i>
                                        Semua
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=berita'>
                                        <i class='bx bx-chevron-right'></i>
                                        Berita
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=ekonomi'>
                                        <i class='bx bx-chevron-right'></i>
                                        Ekonomi
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=olahraga'
                                       >
                                        <i class='bx bx-chevron-right'></i>
                                        Olahraga
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=hiburan'>
                                        <i class='bx bx-chevron-right'></i>
                                        Hiburan
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=talkshow'>
                                        <i class='bx bx-chevron-right'></i>
                                        Talkshow
                                    </a>
                                </li>
                                <li>
                                    <a href='index.php?kategori=video'>
                                        <i class='bx bx-chevron-right'></i>
                                        Video
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget pl-5">
                            <h3>Berita Menarik</h3>
                            <ul class="footer-blog">
                                <?php $perulangan = 0?>
                                <?php foreach($kumpulan_data as $hasil):?>
                                    <?php $perulangan += 1?>
                                    <?php if( $perulangan <= 2 ):?>
                                    <li>
                                        <a href="blog-details.php?id=<?= $hasil["id"]?>">
                                            <img src="assets/gambar/<?= $hasil["gambar"]?>" alt="Images">
                                        </a>
                                        <div class="content">
                                            <h3><a href="blog-details.php?id=<?= $hasil["id"]?>"><?= $hasil["judul"]?></a>
                                            </h3>
                                            <span><?= $hasil["tanggal"]?></span>
                                        </div>
                                    </li>
                                    <?php endif?>
                                <?php endforeach?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget">
                            <h3>Hubungi Kami</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum finibus molestie
                                molestie. Phasellus ac rutrum massa, et volutpat nisl. Fusce ultrices suscipit nisl.</p>
                            <div class="newsletter-area">
                                <form class="newsletter-form" data-toggle="validator" method="POST">
                                    <input type="email" class="form-control" placeholder="Enter Your Email" name="EMAIL"
                                        required autocomplete="off">
                                    <button class="subscribe-btn" type="submit">
                                        <i class='bx bx-paper-plane'></i>
                                    </button>
                                    <div id="validator-newsletter" class="form-result"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy-right-area">
                <div class="copy-right-text">
                    <p>
                        Copyright © <script>
                            document.write(new Date().getFullYear())
                        </script> Ladi.TV All Rights Reserved by
                        <a href="" target="_blank">Ladi Team</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <div class="switch-box">
        <label id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </div>


    <script src="assets/js/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/owl.carousel.min.js"></script>

    <script src="assets/js/jquery.magnific-popup.min.js"></script>

    <script src="assets/js/jquery.nice-select.min.js"></script>

    <script src="assets/js/wow.min.js"></script>

    <script src="assets/js/meanmenu.js"></script>

    <script src="assets/js/jquery.ajaxchimp.min.js"></script>

    <script src="assets/js/form-validator.min.js"></script>

    <script src="assets/js/contact-form-script.js"></script>

    <script src="assets/js/custom.js"></script>

</body>

</html>