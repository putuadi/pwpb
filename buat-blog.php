<?php

require("koneksi.php");

if( isset($_POST["submit"]) ){
    $penulis = $_POST["penulis"];
    $tanggal = $_POST["tanggal"];
    $judul = $_POST["judul"];
    $isi = $_POST["isi"];
    
    $nama_gambar = $_FILES["gambar"]["name"];
    $tmp_name = $_FILES["gambar"]["tmp_name"];

    $ekstensi_gambar = explode('.', $nama_gambar);
    $ekstensi_gambar = strtolower(end($ekstensi_gambar));

    $nama_baru = uniqid();
    $nama_baru .= '.';
    $nama_baru .= $ekstensi_gambar;

    $nama_gambar_baru = $nama_baru;
    
    move_uploaded_file($tmp_name, "assets/gambar/" . $nama_gambar_baru);

    $kategori = $_POST["kategori"];
    
    mysqli_query($koneksi, "INSERT INTO konten_artikel VALUES('', '$penulis', '$tanggal', '$judul', '$isi', '$nama_gambar_baru', '$kategori', 0)");
    
    echo"<script> alert('Data berhasil di buat')
        window.location.href = 'admin.php';
    </script>";
}

?>

<!doctype html>
<html lang="zxx">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/animate.min.css">

    <link rel="stylesheet" href="assets/fonts/flaticon.css">

    <link rel="stylesheet" href="assets/css/boxicons.min.css">

    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <link rel="stylesheet" href="assets/css/nice-select.min.css">

    <link rel="stylesheet" href="assets/css/meanmenu.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" href="assets/css/theme-dark.css">

    <script src="https://cdn.ckeditor.com/4.20.2/standard/ckeditor.js"></script>

    <link rel="icon" type="image/png" href="assets/images/favicon.png">
    <title>Techex - Technology & IT Services HTML Template</title>
</head>

<body>

    <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="spinner"></div>
            </div>
        </div>
    </div>


    <header class="top-header top-header-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-6">
                    <div class="top-head-left">
                        <div class="top-contact">
                            <h3>Dibuat Oleh : <a href="">Adi & Labe</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="top-header-right">
                        <div class="top-header-social">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/" target="_blank">
                                        <i class='bx bxl-facebook'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/?lang=en" target="_blank">
                                        <i class='bx bxl-twitter'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/" target="_blank">
                                        <i class='bx bxl-linkedin-square'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <i class='bx bxl-instagram'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="language-list">
                            <select class="language-list-item">
                                <option>English</option>
                                <option>العربيّة</option>
                                <option>Deutsch</option>
                                <option>Português</option>
                                <option>简体中文</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="navbar-area">

        <div class="mobile-nav">
            <a href="admin.php" class="logo">
                <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo">
                <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light ">
                    <a class="navbar-brand" href="admin.php">
                        <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo" width="260px">
                        <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo" width="260px">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item">
                                <a href="admin.php" class="nav-link active">
                                    Beranda
                                </a>
                            </li>
                        </ul>
                        <div class="nav-side d-display">
                            <div class="nav-side-item">
                                <div class="search-box">
                                    <i class='bx bx-search'></i>
                                </div>
                            </div>
                            <div class="nav-side-item">
                                <div class="get-btn">
                                    <a href="edit-admin.php" class="default-btn btn-bg-two border-radius-50">Profil
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="side-nav-responsive">
            <div class="container-max">
                <div class="dot-menu">
                    <div class="circle-inner">
                        <div class="in-circle circle-one"></div>
                        <div class="in-circle circle-two"></div>
                        <div class="in-circle circle-three"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="side-nav-inner">
                        <div class="side-nav justify-content-center align-items-center">
                            <div class="side-nav-item nav-side">
                                <div class="search-box">
                                    <i class='bx bx-search'></i>
                                </div>
                                <div class="get-btn">
                                    <a href="contact.html" class="default-btn btn-bg-two border-radius-50">Get A Quote
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="search-overlay">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-close">
                    <span class="search-close-line"></span>
                    <span class="search-close-line"></span>
                </div>
                <div class="search-form">
                    <form>
                        <input type="text" class="input-search" placeholder="Search here...">
                        <button type="submit"><i class='bx bx-search'></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="inner-banner">
        <div class="container">
            <div class="inner-title text-center">
                <h3>Tambahkan Blog</h3>
                <ul>
                    <li>
                        <a href="admin.php">Home</a>
                    </li>
                    <li>
                        <i class='bx bx-chevrons-right'></i>
                    </li>
                    <li>Tambahkan Blog</li>
                </ul>
            </div>
        </div>
        <div class="inner-shape">
            <img src="assets/images/shape/inner-shape.png" alt="Images">
        </div>
    </div>


    <div class="user-area pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="user-img">
                        <img src="assets/images/blog/tambah-blog.png" alt="Images">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="user-form">
                        <div class="contact-form">
                            <h2>Tambahkan Blog</h2>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="text" name="penulis" class="form-control" required
                                                data-error="Tolong tulis penulis blog terlebih dahulu"
                                                placeholder="Diposting oleh" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="date" name="tanggal" class="form-control" required
                                                data-error="Tolong isikan tanggal blog terlebih dahulu"
                                                placeholder="Tulis tanggal blog" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="text" name="judul" class="form-control" required
                                                data-error="Tolong tulis judul blog terlebih dahulu"
                                                placeholder="Tulis judul blog" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <textarea name="isi" id="isi" cols="30" rows="10"
                                                class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="file" name="gambar" id="upload" class="form-control" required
                                                data-error="Upload gambar terlebih dahulu" accept="Image/*" />
                                            <div id="display-image"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 mb-3">
                                        <div class="form-group">
                                            <select class="form-select tag-ladi d-none justify-content-center align-items-center" name="kategori" aria-label="Default select example" required>
                                                <option selected>Pilih kategori</option>
                                                <option value="berita">Berita</option>
                                                <option value="ekonomi">Ekonomi</option>
                                                <option value="olahraga">Olahraga</option>
                                                <option value="hiburan">Hiburan</option>
                                                <option value="talkshow">Talkshow</option>
                                                <option value="video">Video</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-condition">
                                        <div class="agree-label">
                                            <input type="checkbox" id="chb1" required/>
                                            <label for="chb1">
                                                Sudah yakin dengan isi konten nya?
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button type="submit" name="submit" class="default-btn btn-bg-two">
                                            Tambah Blog
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="switch-box">
        <label id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </div>


    <script src="assets/js/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/owl.carousel.min.js"></script>

    <script src="assets/js/jquery.magnific-popup.min.js"></script>

    <script src="assets/js/jquery.nice-select.min.js"></script>

    <script src="assets/js/wow.min.js"></script>

    <script src="assets/js/meanmenu.js"></script>

    <script src="assets/js/jquery.ajaxchimp.min.js"></script>

    <script src="assets/js/form-validator.min.js"></script>

    <script src="assets/js/contact-form-script.js"></script>

    <script src="assets/js/custom.js"></script>

    <script>
        CKEDITOR.replace('isi');
    </script>
</body>

</html>