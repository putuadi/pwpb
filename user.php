<?php
session_start();
require("koneksi.php");
$ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel ORDER BY id DESC");
if( $_SESSION["login"] != $_GET["id"]){
    header("Location: index.php");
}

$data_tampil = 3;
$jumlah_halaman = ceil(mysqli_num_rows($ambil_data) / $data_tampil);
if( isset($_GET["halaman"]) ){
    $halaman = $_GET["halaman"];
    $halaman_show = $_GET["halaman"];
    $halaman *= 3;
    $halaman -= $data_tampil;
    $ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel ORDER BY id DESC LIMIT $halaman, $data_tampil");
}else{
    $halaman = 1;
    $halaman_show = 1;
    $halaman *= 3;
    $halaman -= $data_tampil;
    $ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel ORDER BY id DESC LIMIT $halaman, $data_tampil");
}

$cek_kategori = 0;

if(isset($_GET['kategori'])){
    $cek_kategori = 1;
    $kategori_berita = $_GET["kategori"];
    $ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel WHERE kategori='$kategori_berita' ORDER BY id DESC");
    $jumlah_halaman = ceil(mysqli_num_rows($ambil_data) / $data_tampil);
}

if( isset($_POST["cari"]) ){
    $teks_cari = $_POST["teks_cari"];
    $ambil_data = mysqli_query($koneksi, "SELECT * FROM konten_artikel WHERE penulis LIKE '%$teks_cari%' OR tanggal LIKE '%$teks_cari%' OR judul LIKE '%$teks_cari%' OR isi LIKE '%$teks_cari%' OR kategori LIKE '%$teks_cari%'");
    $cek = mysqli_num_rows($ambil_data);
    if( empty($cek) ){
        echo "
        <script>
            alert('Tidak ada');
        </script>";
    }
}

$kumpulan_data = [];

while( $data = mysqli_fetch_assoc($ambil_data) ){
    $kumpulan_data[] = $data;
}


$kumpulan_data_user = [];
$ambil_id_user = $_GET["id"];
$data_user_ini = mysqli_query($koneksi, "SELECT * FROM user_artikel WHERE id=$ambil_id_user");
while( $data_user = mysqli_fetch_assoc($data_user_ini) ){
    $kumpulan_data_user[] = $data_user;
}
foreach( $kumpulan_data_user as $hasil_user ){
    $data_user_gambar = $hasil_user["gambar"];
    $data_user_id = $hasil_user["id"];
}

?>


<!doctype html>
<html lang="zxx">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" href="assets/css/animate.min.css">

    <link rel="stylesheet" href="assets/fonts/flaticon.css">

    <link rel="stylesheet" href="assets/css/boxicons.min.css">

    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <link rel="stylesheet" href="assets/css/nice-select.min.css">

    <link rel="stylesheet" href="assets/css/meanmenu.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link rel="stylesheet" href="assets/css/responsive.css">

    <link rel="stylesheet" href="assets/css/theme-dark.css">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">
    <title>Techex - Technology & IT Services HTML Template</title>
</head>

<body>

    <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="spinner"></div>
            </div>
        </div>
    </div>


    <header class="top-header top-header-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7 col-md-6">
                    <div class="top-head-left">
                        <div class="top-contact">
                            <h3>Dibuat Oleh : <a href="">Adi & Labe</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6">
                    <div class="top-header-right">
                        <div class="top-header-social">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/" target="_blank">
                                        <i class='bx bxl-facebook'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/?lang=en" target="_blank">
                                        <i class='bx bxl-twitter'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/" target="_blank">
                                        <i class='bx bxl-linkedin-square'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">
                                        <i class='bx bxl-instagram'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="language-list">
                            <select class="language-list-item">
                                <option>English</option>
                                <option>العربيّة</option>
                                <option>Deutsch</option>
                                <option>Português</option>
                                <option>简体中文</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


    <div class="navbar-area">

        <div class="mobile-nav">
            <a href="user.php?id=<?=$ambil_id_user?>" class="logo">
                <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo">
                <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo">
            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light ">
                    <a class="navbar-brand" href="user.php?id=<?=$ambil_id_user?>">
                        <img src="assets/images/logos/logoladi-dark.png" class="logo-one" alt="Logo" style="width: 260px;">
                        <img src="assets/images/logos/logoladi-white.png" class="logo-two" alt="Logo" style="width: 260px;">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item">
                                <a href="user.php?id=<?=$ambil_id_user?>" class="nav-link <?= (!isset($_GET['kategori']))? 'active' : ''?>">
                                    Semua
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=berita&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'berita')? 'active' : ''?>">
                                    Berita
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=ekonomi&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'ekonomi')? 'active' : ''?>">
                                    Ekonomi
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=olahraga&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'olahraga')? 'active' : ''?>">
                                    Olahraga
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=hiburan&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'hiburan')? 'active' : ''?>">
                                    Hiburan
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=talkshow&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'talkshow')? 'active' : ''?>">
                                    Talkshow
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href='user.php?kategori=video&id=<?=$ambil_id_user?>' class="nav-link <?= (isset($_GET['kategori']) && $_GET['kategori'] == 'video')? 'active' : ''?>">
                                    Video
                                </a>
                            </li>
                        </ul>
                        <div class="nav-side d-display d-flex justify-content-center align-items-center">
                            <div class="nav-side-item">
                                <div class="search-box mt-3">
                                    <i class='bx bx-search'></i>
                                </div>
                            </div>
                            <div class="nav-side-item">
                                <div class="get-btn">
                                    <a href="edit-user.php?id=<?= $data_user_id?>"><img src="assets/user/<?= $data_user_gambar?>" width="60px" height="60px" style="border-radius: 50%">
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="side-nav-responsive">
            <div class="container-max">
                <div class="dot-menu">
                    <div class="circle-inner">
                        <div class="in-circle circle-one"></div>
                        <div class="in-circle circle-two"></div>
                        <div class="in-circle circle-three"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="side-nav-inner">
                        <div class="side-nav justify-content-center align-items-center">
                            <div class="side-nav-item nav-side">
                                <div class="search-box">
                                    <i class='bx bx-search'></i>
                                </div>
                                <div class="get-btn">
                                    <a href="edit-user.php?id=<?= $data_user_id?>"><img src="assets/user/<?= $data_user_gambar?>" width="60px" height="55px" style="border-radius: 50%">
                                        <i class='bx bx-chevron-right'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="search-overlay">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-layer"></div>
                <div class="search-close">
                    <span class="search-close-line"></span>
                    <span class="search-close-line"></span>
                </div>
                <div class="search-form">
                    <form action="" method="post">
                        <input type="text" class="input-search" name="teks_cari" placeholder="Search here...">
                        <button type="submit" name="cari"><i class='bx bx-search'></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="inner-banner">
        <div class="container">
            <div class="inner-title text-center">
                <h3>Selamat datang di Ladi.TV</h3>
                <ul>
                    <li>ladi.TV menyajikan berita breaking news dan berita terkini</li>
                </ul>
            </div>
        </div>
        <div class="inner-shape">
            <img src="assets/images/shape/inner-shape.png" alt="Images">
        </div>
    </div>


    <div class="blog-style-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <?php if(empty(mysqli_num_rows($ambil_data))):?>
                            <h1><?= "Maaf, data dengan kategori ini belum ada"?></h1>
                        <?php endif?>
                        <?php foreach($kumpulan_data as $hasil):?>
                            <div class="col-lg-12">
                                <div class="blog-style-card">
                                    <div class="blog-style-img">
                                        <a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>">
                                            <img src="assets/gambar/<?= $hasil["gambar"]?>" alt="Images">
                                        </a>
                                        <div class="blog-style-tag">
                                            <h3><?= $hasil["tanggal"]?></h3>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <ul>
                                            <li><i class='bx bxs-user'></i> <?= $hasil["penulis"]?></li>
                                            <li><i class='bx bx-show-alt'></i><?= $hasil["view"]?> View</li>
                                            <li><i class='bx bx-purchase-tag-alt'></i><?= $hasil["kategori"]?></li>
                                        </ul>
                                        <h3><a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>"><?= $hasil["judul"]?></a>
                                        </h3>
                                        <p><?= substr($hasil["isi"], 0,450)?>...</p>
                                        <a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>" class="default-btn btn-bg-two border-radius-50">Learn
                                            More <i class='bx bx-chevron-right'></i></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach?>
                        <div class="col-lg-12 col-md-12 text-center">
                            <div class="pagination-area">
                            <a href="user.php?halaman=<?= ($halaman - 2 < 0)? '1' : $halaman - 2?><?= ($cek_kategori == 1)? "&kategori=$kategori_berita" : ""?>&id=<?=$ambil_id_user?>" class="prev page-numbers">
                            <i class="bx bx-left-arrow-alt"></i>
                            </a>
                            <?php for($i = 1; $i <= $jumlah_halaman; $i++):?>
                                <a href="user.php?halaman=<?=$i?><?= ($cek_kategori == 1)? '&kategori=$kategori_berita' : ''?>&id=<?=$ambil_id_user?>" class="page-numbers <?= ($i == $halaman_show)? 'current' : ''?>"><?= $i?></a>
                            <?php endfor?>
                            <a href="user.php?halaman=<?=$halaman + 2?><?= ($cek_kategori == 1)? "&kategori=$kategori_berita" : ""?>&id=<?=$ambil_id_user?>" class="next page-numbers">
                            <i class="bx bx-right-arrow-alt"></i>
                            </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="side-bar-area">
                        <div class="search-widget">
                            <form class="search-form">
                                <input type="search" class="form-control" placeholder="Search...">
                                <button type="submit">
                                    <i class="bx bx-search"></i>
                                </button>
                            </form>
                        </div>
                        <div class="side-bar-widget">
                            <h3 class="title">Blog Categories</h3>
                            <div class="side-bar-categories">
                                <ul>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php'>Semua</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=berita&id=<?=$ambil_id_user?>'>Berita</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=ekonomi&id=<?=$ambil_id_user?>'>Ekonomi</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=olahraga&id=<?=$ambil_id_user?>'>Olahraga</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=hiburan&id=<?=$ambil_id_user?>'>Hiburan</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=talkshow&id=<?=$ambil_id_user?>'>Talkshow</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=video&id=<?=$ambil_id_user?>'>Video</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="side-bar-widget">
                            <h3 class="title">Blog Terbaru</h3>
                            <div class="widget-popular-post">
                                <?php $perulangan = 0?>
                                <?php foreach($kumpulan_data as $hasil):?>
                                    <?php $perulangan += 1?>
                                    <?php if( $perulangan <= 4 ):?>
                                    <article class="item">
                                        <a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>"
                                            class="thumb">
                                            <span class="full-image cover bg1" role="img" style="background-image: url(assets/gambar/<?= $hasil["gambar"]?>) !important"></span>
                                        </a>
                                        <div class="info">
                                            <h4 class="title-text">
                                                <a
                                                    href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>">
                                                    <?= $hasil["judul"]?>
                                                </a>
                                            </h4>
                                            <p><?= $hasil["tanggal"]?></p>
                                        </div>
                                    </article>
                                    <?php endif?>
                                <?php endforeach?>
                            </div>
                        </div>
                        <div class="side-bar-widget">
                            <h3 class="title">Tag Cloud</h3>
                            <ul class="side-bar-widget-tag">
                                <li><a href='user.php'>Semua</a></li>
                                <li><a href='user.php?kategori=berita&id=<?=$ambil_id_user?>'>Berita</a></li>
                                <li><a href='user.php?kategori=ekonomi&id=<?=$ambil_id_user?>'>Ekonomi</a></li>
                                <li><a href='user.php?kategori=olahraga&id=<?=$ambil_id_user?>'>Olahraga</a></li>
                                <li><a href='user.php?kategori=hiburan&id=<?=$ambil_id_user?>'>Hiburan</a></li>
                                <li><a href='user.php?kategori=talkshow&id=<?=$ambil_id_user?>'>Talkshow</a></li>
                                <li><a href='user.php?kategori=video&id=<?=$ambil_id_user?>'>Video</a></li>
                            </ul>
                        </div>
                        <div class="side-bar-widget">
                            <h3 class="title">Gallery</h3>
                            <ul class="blog-gallery">
                                <?php $perulangan = 0?>
                                <?php foreach($kumpulan_data as $hasil):?>
                                    <?php $perulangan += 1?>
                                    <?php if( $perulangan <= 6 ):?>
                                    <li>
                                        <a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>">
                                            <img src="assets/gambar/<?= $hasil["gambar"]?>" alt="image">
                                        </a>
                                    </li>
                                    <?php endif?>
                                <?php endforeach?>
                            </ul>
                        </div>
                        <div class="side-bar-widget">
                            <h3 class="title">Archive</h3>
                            <div class="side-bar-categories">
                                <ul>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php'>Semua</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=berita&id=<?=$ambil_id_user?>'>Berita</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=ekonomi&id=<?=$ambil_id_user?>'>Ekonomi</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=olahraga&id=<?=$ambil_id_user?>'>Olahraga</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=hiburan&id=<?=$ambil_id_user?>'>Hiburan</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=talkshow&id=<?=$ambil_id_user?>'>Talkshow</a>
                                    </li>
                                    <li>
                                        <div class="line-circle"></div>
                                        <a href='user.php?kategori=video&id=<?=$ambil_id_user?>'>Video</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer-area footer-bg">
        <div class="container">
            <div class="footer-top pt-100 pb-70">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="footer-widget">
                            <div class="footer-logo">
                                <a href="user.php?id=<?=$ambil_id_user?>">
                                    <img src="assets/images/logos/logoladi-white.png" alt="Images" width="260px">
                                </a>
                            </div>
                            <p>
                                Ladi.TV menyajikan berita breaking news dan berita terkini seputar nasional,
                                    internasional, politik, ekonomi,
                                    olahraga, dan entertainment
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="footer-widget pl-2">
                            <h3>Kategori</h3>
                            <ul class="footer-list">
                                <li>
                                    <a href='user.php'>
                                        <i class='bx bx-chevron-right'></i>
                                        Semua
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=berita&id=<?=$ambil_id_user?>'>
                                        <i class='bx bx-chevron-right'></i>
                                        Berita
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=ekonomi&id=<?=$ambil_id_user?>'>
                                        <i class='bx bx-chevron-right'></i>
                                        Ekonomi
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=olahraga&id=<?=$ambil_id_user?>'
                                       >
                                        <i class='bx bx-chevron-right'></i>
                                        Olahraga
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=hiburan&id=<?=$ambil_id_user?>'>
                                        <i class='bx bx-chevron-right'></i>
                                        Hiburan
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=talkshow&id=<?=$ambil_id_user?>'>
                                        <i class='bx bx-chevron-right'></i>
                                        Talkshow
                                    </a>
                                </li>
                                <li>
                                    <a href='user.php?kategori=video&id=<?=$ambil_id_user?>'>
                                        <i class='bx bx-chevron-right'></i>
                                        Video
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget pl-5">
                            <h3>Berita Menarik</h3>
                            <ul class="footer-blog">
                                <?php $perulangan = 0?>
                                <?php foreach($kumpulan_data as $hasil):?>
                                    <?php $perulangan += 1?>
                                    <?php if( $perulangan <= 2 ):?>
                                    <li>
                                        <a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>">
                                            <img src="assets/gambar/<?= $hasil["gambar"]?>" alt="Images">
                                        </a>
                                        <div class="content">
                                            <h3><a href="blog-details-user.php?id=<?= $hasil["id"]?>&userid=<?=$ambil_id_user?>"><?= $hasil["judul"]?></a>
                                            </h3>
                                            <span><?= $hasil["tanggal"]?></span>
                                        </div>
                                    </li>
                                    <?php endif?>
                                <?php endforeach?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="footer-widget">
                            <h3>Hubungi Kami</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum finibus molestie
                                molestie. Phasellus ac rutrum massa, et volutpat nisl. Fusce ultrices suscipit nisl.</p>
                            <div class="newsletter-area">
                                <form class="newsletter-form" data-toggle="validator" method="POST">
                                    <input type="email" class="form-control" placeholder="Enter Your Email" name="EMAIL"
                                        required autocomplete="off">
                                    <button class="subscribe-btn" type="submit">
                                        <i class='bx bx-paper-plane'></i>
                                    </button>
                                    <div id="validator-newsletter" class="form-result"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy-right-area">
                <div class="copy-right-text">
                    <p>
                        Copyright © <script>
                            document.write(new Date().getFullYear())
                        </script> Ladi.TV All Rights Reserved by
                        <a href="" target="_blank">Ladi Team</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>


    <div class="switch-box">
        <label id="switch" class="switch">
            <input type="checkbox" onchange="toggleTheme()" id="slider">
            <span class="slider round"></span>
        </label>
    </div>


    <script src="assets/js/jquery.min.js"></script>

    <script src="assets/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/owl.carousel.min.js"></script>

    <script src="assets/js/jquery.magnific-popup.min.js"></script>

    <script src="assets/js/jquery.nice-select.min.js"></script>

    <script src="assets/js/wow.min.js"></script>

    <script src="assets/js/meanmenu.js"></script>

    <script src="assets/js/jquery.ajaxchimp.min.js"></script>

    <script src="assets/js/form-validator.min.js"></script>

    <script src="assets/js/contact-form-script.js"></script>

    <script src="assets/js/custom.js"></script>
</body>

</html>